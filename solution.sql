-- find all artists that has letter "d" in their name
SELECT * FROM artists WHERE name LIKE "d%";
SELECT * FROM artists WHERE name LIKE "%d%";
SELECT * FROM artists WHERE name LIKE "%d";

-- find all songs that has length of less than 230
SELECT * FROM songs WHERE length < 230;


--------------------------------------
--                                  --
--                                  --
--                                  --
--                                  --
--   SIR KULANG DATA KO SA SONGS    --
--                                  --
--                                  --
--                                  --
--                                  --
--------------------------------------

-- Join albums and songs table (show only album name, song name, song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- Join artists and albums table (only with letter a in the album names)
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "%a%";

-- Sort albums in Z-A order ( show only first 4)
SELECT album_title FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join albums and songs table (Z-A order)
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;